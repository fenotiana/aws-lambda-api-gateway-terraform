"use strict";

const users = [
  {
    id: 1,
    firstname: "Mil",
    lastname: "Sorsby",
    email: "msorsby0@infoseek.co.jp",
  },
  {
    id: 2,
    firstname: "Dale",
    lastname: "Canacott",
    email: "dcanacott1@yahoo.com",
  },
  {
    id: 3,
    firstname: "Brittaney",
    lastname: "Beswetherick",
    email: "bbeswetherick2@economist.com",
  },
  {
    id: 4,
    firstname: "Cash",
    lastname: "Crankhorn",
    email: "ccrankhorn3@usnews.com",
  },
  {
    id: 5,
    firstname: "Glynn",
    lastname: "Renfrew",
    email: "grenfrew4@java.com",
  },
  {
    id: 6,
    firstname: "Luis",
    lastname: "Maberley",
    email: "lmaberley5@craigslist.org",
  },
  {
    id: 7,
    firstname: "Lisabeth",
    lastname: "Adamovitz",
    email: "ladamovitz6@adobe.com",
  },
  {
    id: 8,
    firstname: "Keen",
    lastname: "Chark",
    email: "kchark7@pbs.org",
  },
  {
    id: 9,
    firstname: "Cleve",
    lastname: "Gwynne",
    email: "cgwynne8@zdnet.com",
  },
  {
    id: 10,
    firstname: "Gladys",
    lastname: "Samett",
    email: "gsamett9@dailymotion.com",
  },
  {
    id: 11,
    firstname: "Elene",
    lastname: "Spong",
    email: "esponga@bloomberg.com",
  },
  {
    id: 12,
    firstname: "Freddy",
    lastname: "Quilliam",
    email: "fquilliamb@whitehouse.gov",
  },
  {
    id: 13,
    firstname: "Haydon",
    lastname: "Fettiplace",
    email: "hfettiplacec@hao123.com",
  },
  {
    id: 14,
    firstname: "Enriqueta",
    lastname: "Peggs",
    email: "epeggsd@tumblr.com",
  },
  {
    id: 15,
    firstname: "Nanny",
    lastname: "Haversum",
    email: "nhaversume@friendfeed.com",
  },
  {
    id: 16,
    firstname: "Adriaens",
    lastname: "Colbeck",
    email: "acolbeckf@live.com",
  },
  {
    id: 17,
    firstname: "Pryce",
    lastname: "Hellard",
    email: "phellardg@mayoclinic.com",
  },
  {
    id: 18,
    firstname: "Alejandrina",
    lastname: "Frotton",
    email: "afrottonh@nature.com",
  },
  {
    id: 19,
    firstname: "Isador",
    lastname: "Huggan",
    email: "ihuggani@github.io",
  },
  {
    id: 20,
    firstname: "Dale",
    lastname: "Feehily",
    email: "dfeehilyj@biblegateway.com",
  },
];

exports.handler = function (event, context, callback) {
  var response = {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({ data: users }),
  };
  
  callback(null, response);
};
