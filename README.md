# AWS Lambda & AWS API Gateway & Terraform

Cloner ce projet: 
`git clone https://gitlab.com/fenotiana/aws-lambda-api-gateway-terraform.git`

### Pré-requis: 
- Configurer les variables d'environnement sur votre OS: **AWS_ACCESS_KEY_ID**,
**AWS_SECRET_ACCESS_KEY**
- Zipper le fichier **main.js** et le nom du fichier zip doit être **lambda.zip**

### Initialisation et lancement
- Initialiser l'app: `terraform init`
- Vérifier avant déploiement: `terraform plan`
- Si tout est OK, déployer: `terraform apply`
